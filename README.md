### Golang Plugin Example

Golangのプラグイン機能を使ってみるテスト

### 実行方法

1. dockerでコンテナを立てます  
    `docker-compose up -d --build`
2. たてたコンテナ内で英語用ファイルをプラグイン化  
    `docker exec golang-plugin-example go build -buildmode=plugin -o plugins/en.so en/main.go`
3. 日本語も同様にプラグイン化  
    `docker exec golang-plugin-example go build -buildmode=plugin -o plugins/ja.so ja/main.go`
4. 引数なしならデフォルトの日本語が  
    `docker exec golang-plugin-example go run main.go`
5. 日本語を明示的に指定することも  
    `docker exec golang-plugin-example go run main.go -lang=ja`
6. 英語を指定すると英語で  
    `docker exec golang-plugin-example go run main.go -lang=en`


## 動的ロード

1. dockerでコンテナを立てます  
    `docker-compose up -d --build`
2. たてたコンテナ内で英語用ファイルをプラグイン化  
    `docker exec golang-plugin-example go build -buildmode=plugin -o plugins/en.so en/main.go`
3. 日本語プラグインを削除しておきます  
    `docker exec golang-plugin-example rm -f plugins/ja.so`
4. keepフラグをtrueにして実行すると、プラグインファイルがないので待つと表示されます  
    `docker exec golang-plugin-example go run main.go -keep=true`
5. jaのファイルをプラグインとしてビルドすると、ファイルが出来たところでロードが走って処理が完了します    
    `docker exec golang-plugin-example go build -buildmode=plugin -o plugins/ja.so ja/main.go`

ちなみに、一度開かれたプラグインはファイルロックされるので、途中で書き換えるなどはできません。  
必要なプラグインを必要な時に読み込むための動的ロードですね。
