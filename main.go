package main

import (
	"flag"
	"fmt"
	"log"
	"plugin"
	"time"
)

var lang string
var keep bool

func init() {
	l := flag.String("lang", "ja", "language")
	k := flag.Bool("keep", false, "keep")
	flag.Parse()

	lang = *l
	keep = *k

	log.Println(lang, keep)
}

func main() {
	plug, err := plugin.Open(fmt.Sprintf("./plugins/%s.so", lang))
	if err != nil {

		if keep {
			log.Printf("./plugins/%s.so is not found. and wait\n", lang)
			time.Sleep(3 * time.Second)
			main()
			return
		}

		log.Fatalf("./plugins/%s.so is not found\n", lang)
	}

	if language, err := plug.Lookup("Language"); err != nil {
		log.Fatalf("Language is not exists\n")
	} else {
		fmt.Printf("Language: %s\n", *language.(*string))
	}

	if greeting, err := plug.Lookup("Greeting"); err != nil {
		log.Fatalf("Greeting is not exists\n")
	} else {
		fmt.Printf("Greeting: %s\n", *greeting.(*string))
	}
}
